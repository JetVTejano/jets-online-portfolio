// Show and Hide Menu
const navMenu = document.getElementById('nav-menu'),
    toggleMenu = document.getElementById('nav-toggle'),
    closeMenu = document.getElementById('nav-close')

// Show
toggleMenu.addEventListener('click', ()=>{
    navMenu.classList.toggle('show')
})

// Hide
closeMenu.addEventListener('click', ()=>{
    navMenu.classList.remove('show')
})

// Remove Menu
const navLink = document.querySelectorAll('.nav__link')

function linkAction(){
    navMenu.classList.remove('show');
}
navLink.forEach(n => n.addEventListener('click', linkAction));

// Scroll Sections Active Link
const sections = document.querySelectorAll('section[id]')

window.addEventListener('scroll', scrollActive)

function scrollActive(){
    const scrollY = window.pageYOffset

    sections.forEach(current =>{
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop - 50
        sectionId = current.getAttribute('id')

        if(scrollY > sectionTop && scrollY <= sectionTop +sectionHeight){
            document.querySelector('.nav__menu a[href*='+ sectionId +']').classList.add('active')
        }else{
            document.querySelector('.nav__menu a[href*='+ sectionId +']').classList.remove('active')
        }
    })
}

// Scroll Reveal
const sr = ScrollReveal({
    origin: 'bottom',
    distance: '80px',
    duration: 2000,
    reset: false
})

// Home Reveals
sr.reveal('.home__img', {})
sr.reveal('.home__title', {delay: 200})
sr.reveal('.home__profession', {delay: 300})
sr.reveal('.home__social', {delay: 400})
sr.reveal('.button', {delay: 500})

// Section Reveals
sr.reveal('.section-subtitle', {delay:50})
sr.reveal('.section-title', {delay:100})

// About Reveals
sr.reveal('.about__information', {delay: 300})
sr.reveal('.about__data', {delay: 300})

// Skills Reveals
sr.reveal('.skills__subtitle', {delay: 300})
sr.reveal('.skills__data', {delay: 400})
sr.reveal('.skills__img', {delay: 500})

// Works Reveals
sr.reveal('.works__img', {delay: 300})

// Contact Reveals
sr.reveal('.contact__form', {delay: 300})
sr.reveal('.contact__data', {delay: 300})